/**
 * 
 */
package grapher.ui;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import grapher.fc.Function;
import grapher.fc.FunctionFactory;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;

/**
 * @author ch
 *
 */
public class Menu extends JPanel implements ActionListener, MouseListener, ListSelectionListener {
	JList<Function> List;
	String[] data;
	String exp_to_add;
	String exp_to_delete;
	JToolBar tb;
	private JButton minus;
	private JButton add;
	Grapher grapher;
	boolean itemsSelected;
	private DefaultListModel<Function> demoList;
	private DialogBox d1;
	private DialogBox d2;

	public Menu(Grapher g) {
		grapher = g;
		grapher.setMenu(this);
		tb = new JToolBar();
		List = new JList<Function>(grapher.functions);
		this.setLayout(new BorderLayout());
		add(List, BorderLayout.CENTER);

		add(tb, BorderLayout.SOUTH);
		minus = new JButton("-");
		tb.add(minus);
		add = new JButton("+");
		tb.add(add);

		minus.addActionListener(this);
		add.addActionListener(this);
		addMouseListener(this);
		List.addMouseListener(this);
		List.addListSelectionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == minus) {
			d1 = new DialogBox();
			if (d1.expression != null || d1.expression!="") {
				
			try {
				
				grapher.remove(FunctionFactory.createFunction(d1.expression));
				demoList.removeElement(FunctionFactory.createFunction(d1.expression));
			}
			catch (RuntimeException t) {
				
			}
			demoList.removeElement(d1.expression);
				
			}
			

		}
		if (e.getSource() == add) {
			d2 = new DialogBox();
			if (d2.expression != null || d2.expression!="") {
			try {
				grapher.add(d2.expression);
				demoList.addElement(FunctionFactory.createFunction(d2.expression));
			}
			catch (RuntimeException t) {
				
			}
				
			}
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void valueChanged(ListSelectionEvent e) {
		repaint();

	}

}
