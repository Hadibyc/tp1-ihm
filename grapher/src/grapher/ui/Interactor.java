package grapher.ui;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class Interactor implements MouseListener, MouseMotionListener, MouseWheelListener {

	Grapher g;
	int x;
	int y;

	enum state {
		Start, LPress, LDrag, LClick, RPress, RDrag, RClick

	}

	state current;

	public Interactor(Grapher g) {
		this.g = g;
		this.current = state.Start;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {

		switch (this.current) {

		case Start:
			if (e.getButton() == MouseEvent.BUTTON1) {
				this.current = state.LPress;

			}
			if (e.getButton() == MouseEvent.BUTTON3) {
				this.current = state.RPress;
				this.g.XCorner1 = e.getX();
				this.g.YCorner1 = e.getY();

			}
			this.x = e.getX();
			this.y = e.getY();
			break;
		default:
			break;

		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {

		switch (this.current) {
		case LDrag:
			Cursor cursor = new Cursor(Cursor.DEFAULT_CURSOR);
			g.setCursor(cursor);
			this.x = e.getX();
			this.y = e.getY();
			this.current = state.Start;
			break;
		case LPress:
			Point p = new Point(this.x, this.y);
			this.g.zoom(p, 5);
			this.x = e.getX();
			this.y = e.getY();
			this.current = state.Start;
			break;
		case RDrag:
			this.g.select = false;
			Point p1 = new Point(this.g.XCorner1, this.g.YCorner1);
			Point p2 = new Point(this.g.XCorner2, this.g.YCorner2);
			this.g.zoom(p1, p2);
			this.x = e.getX();
			this.y = e.getY();
			this.current = state.Start;

			break;
		case RPress:
			p = new Point(this.x, this.y);
			this.g.zoom(p, -5);
			this.x = e.getX();
			this.y = e.getY();
			this.current = state.Start;

			break;

		default:
			break;

		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		this.x = e.getX();
		this.y = e.getY();

	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.x = e.getX();
		this.y = e.getY();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		int dx = x - e.getX();
		int dy = y - e.getY();
		switch (this.current) {
		case LPress:
			Point p = new Point(e.getX(), e.getY());
			if (p.distance(this.x, this.y) > 5) {
				Cursor c = new Cursor(Cursor.HAND_CURSOR);
				g.setCursor(c);
				dx = x - e.getX();
				dy = y - e.getY();
				this.g.translate(-dx, -dy);
				this.x = e.getX();
				this.y = e.getY();
				this.current = state.LDrag;

			}

			break;
		case LDrag:
			this.current = state.LDrag;
			dx = x - e.getX();
			dy = y - e.getY();
			this.g.translate(-dx, -dy);
			this.x = e.getX();
			this.y = e.getY();
			break;
		case RPress:
			Point p2 = new Point(e.getX(), e.getY());
			if (p2.distance(this.x, this.y) > 5) {
				this.g.select = true;
				this.g.XCorner2 = e.getX();
				this.g.YCorner2 = e.getY();
				this.current = state.RDrag;
			}
			break;
		case RDrag:
			this.current = state.RDrag;
			this.g.XCorner2 = e.getX();
			this.g.YCorner2 = e.getY();
			this.g.repaint();
			break;
		default:
			break;

		}
		this.x = e.getX();
		this.y = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		this.x = e.getX();
		this.y = e.getY();

	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		switch (this.current) {
		case Start:
			int n = e.getWheelRotation();

			if (n < 0) {
				Point p = new Point(this.x, this.y);
				this.g.zoom(p, 5);
				this.current = state.Start;

			} else {
				Point p = new Point(this.x, this.y);
				this.g.zoom(p, -5);
				this.current = state.Start;

			}
			break;
		default:
			break;
		}

	}

}
