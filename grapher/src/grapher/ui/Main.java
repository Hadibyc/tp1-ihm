/* grapher.ui.Main
 * (c) blanch@imag.fr 2021–                                                */

package grapher.ui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

// main that launch a grapher.ui.Grapher

public class Main extends JFrame {
	Main(String title, String[] expressions) {
		
		super(title);
		 
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		Grapher grapher = new Grapher();
		Interactor i = new Interactor(grapher);
		
		grapher.addMouseListener(i);
		grapher.addMouseMotionListener(i);
		grapher.addMouseWheelListener(i);
		for(String expression: expressions) {
			grapher.add(expression);
		}
		Menu m =new Menu(grapher);
		 JSplitPane sl = new JSplitPane(SwingConstants.VERTICAL, (JPanel)m ,(JPanel)grapher);
		
	    add(sl);
		pack();
	}

	public static void main(String[] argv) {
		final String[] expressions = argv;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Main("grapher", expressions).setVisible(true);
			}
		});
	}
}
